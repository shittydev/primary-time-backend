from django.apps import AppConfig


class PublicAuthConfig(AppConfig):
    name = 'public_auth'
    verbose_name = 'Пользователи сайта'
