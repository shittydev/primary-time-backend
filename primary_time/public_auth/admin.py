from django.contrib import admin
from public_auth.models import (
    User,
    Subscriber,
)


class UserProfileAdmin(admin.ModelAdmin):
    list_display = (
        'initials', 'phone_number', 'email',
        'is_active', 'is_subscriber'
    )
    list_filter = ('is_active', 'is_subscriber')


class SubscriberAdmin(admin.ModelAdmin):
    list_display = (
        'name', 'phone', 'email',
        'is_auth_user'
    )
    list_filter = ('is_auth_user',)


admin.site.register(User, UserProfileAdmin)
admin.site.register(Subscriber, SubscriberAdmin)
