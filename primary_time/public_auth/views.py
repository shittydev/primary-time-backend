from django.shortcuts import render, render_to_response
from django.views.decorators.csrf import ensure_csrf_cookie
from rest_framework.generics import (
    ListCreateAPIView,
    RetrieveUpdateDestroyAPIView,
)
from public_auth.serializers import (
    SubscriberSerializer,
)

from braces.views import CsrfExemptMixin


def public_auth_index(request):
    return render(request, 'public_auth/index.html')


def public_auth_login(request):
    return render(request, 'public_auth/index.html')


def public_auth_registration(request):
    return render(request, 'public_auth/index.html')


def public_auth_password_recovery(request):
    return render(request, 'public_auth/index.html')


def public_auth_email_confirmation(request, key):
    print(key)
    return render_to_response(
        'public_auth/index.html',
        {'verkey': key}
    )

def public_auth_password_reset(request, uid, token):
    print(uid, token)
    return render_to_response(
        'public_auth/index.html',
        {'resuid': uid, 'restoken': token}
    )


class SubscribersList(CsrfExemptMixin, ListCreateAPIView):
    authentication_classes = []
    serializer_class = SubscriberSerializer
