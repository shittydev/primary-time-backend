from django.db import models
from django.contrib.auth.models import AbstractUser
from phonenumber_field.modelfields import PhoneNumberField


class User(AbstractUser):
    initials = models.CharField(
        max_length=160,
        blank=True,
        default='',
        verbose_name="Инициалы",
    )
    phone_number = models.CharField(
        max_length=160,
        blank=True,
        default='',
        verbose_name="Номер телефона",
    )
    is_subscriber = models.BooleanField(
        blank=True,
        default=False,
        verbose_name="Подписан на рассылку",
    )
    is_admin_email_receiver = models.BooleanField(
        blank=True,
        default=False,
        verbose_name="Получает админские письма",
    )

    def get_full_name(self):
        return self.username


class Subscriber(models.Model):
    name = models.CharField(
        max_length=160,
        blank=False
    )
    phone = PhoneNumberField(
        default='',
        blank=False,
        max_length=160
    )
    email = models.EmailField(
        blank=False,
        default='',
        max_length=255,
        unique=True,
    )
    is_auth_user = models.BooleanField(
        default=False,
    )

    def __str__(self):
        return self.email

    class Meta:
        verbose_name = "Подписчик рассылки"
        verbose_name_plural = "Подписчики рассылки"
        ordering = ['email']
