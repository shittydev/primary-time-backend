from rest_framework import serializers
from rest_auth.registration.serializers import (
    RegisterSerializer
)
from rest_auth.serializers import (
    UserDetailsSerializer,
    PasswordResetConfirmSerializer
)

from allauth.account import app_settings as allauth_settings
from allauth.utils import email_address_exists
from allauth.account.adapter import get_adapter
from allauth.account.utils import setup_user_email

from public_auth.models import (
    User,
    Subscriber,
)


class UserProfileRegisterSerializer(RegisterSerializer):
    initials = serializers.CharField(required=True, write_only=True)
    username = serializers.CharField(required=True, write_only=True)
    phone_number = serializers.CharField(required=True, write_only=True)

    is_subscriber = serializers.BooleanField(required=False)
    # is_admin_email_receiver = serializers.BooleanField(required=False)

    def get_cleaned_data(self):
        return {
            'initials': self.validated_data.get('initials', ''),
            'username': self.validated_data.get('username', ''),
            'password1': self.validated_data.get('password1', ''),

            'phone_number': self.validated_data.get('phone_number', ''),
            'email': self.validated_data.get('email', ''),
            'is_subscriber': self.validated_data.get('is_subscriber', ''),
            # 'is_admin_email_receiver': self.validated_data.get('is_admin_email_receiver', ''),
        }

    def save(self, request):
        adapter = get_adapter()
        user = adapter.new_user(request)
        self.cleaned_data = self.get_cleaned_data()
        
        user.phone_number = self.cleaned_data['phone_number']
        user.initials = self.cleaned_data['initials']
        user.is_subscriber = self.cleaned_data['is_subscriber']
        # user.is_admin_email_receiver = self.cleaned_data['is_admin_email_receiver']

        adapter.save_user(request, user, self)
        setup_user_email(request, user, [])

        # Если при регистрации отмечена подписка
        # Создается инстанс подписчика с таким имейлом
        if user.is_subscriber:
            subscriber = Subscriber(
                name=user.initials,
                phone=user.phone_number,
                email=user.email,
                is_auth_user=True
            )
            
            subscriber.save()
        return user


class UserSerializer(UserDetailsSerializer):
    initials = serializers.CharField(source="user.initials")
    username = serializers.CharField(source="user.username")
    phone_number = serializers.CharField(source="user.phone_number")

    class Meta(UserDetailsSerializer.Meta):
        fields = UserDetailsSerializer.Meta.fields + \
            ('initials', 'username', 'phone_number',)

    def update(self, instance, validated_data):
        profile_data = validated_data.pop('user', {})
        initials = profile_data.get('initials')
        username = profile_data.get('username')
        phone_number = profile_data.get('phone_number')

        instance = super(UserSerializer, self).update(instance, validated_data)

        # get and update user profile
        profile = instance.user
        if profile_data and initials and username and phone_number:
            profile.initials = initials
            profile.username = username
            profile.phone_number = phone_number
            profile.save()
        return instance

# Допилить восстановление пароля
# class PasswordRecoverySerializer(PasswordResetSerializer):
#     def get_email_options(self):
#         data = {
#             'email_template_name': 'email/password_reset.html',
#             'subject_template_name': 'email/password_reset_subject.txt',
#         }
#         return data


class SubscriberSerializer(serializers.ModelSerializer):
    def create(self, validated_data):
        subscriber = Subscriber(
            name=validated_data['name'],
            phone=validated_data['phone'],
            email=validated_data['email'],
            is_auth_user=User.objects.filter(
                email=validated_data['email']).exists()
        )
        subscriber.save()
        return subscriber

    class Meta:
        model = Subscriber
        fields = ('name', 'phone', 'email', 'is_auth_user')
