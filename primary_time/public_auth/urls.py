from django.contrib import admin
from django.urls import path, include, re_path
from . import views


urlpatterns = [
    path(
        '',
        views.public_auth_index,
        name="homepage"
    ),

    path(
        'login/',
        views.public_auth_login,
        name="homepage-login"
    ),
    path(
        'registration/',
        views.public_auth_registration,
        name="homepage-registration"
    ),
    path(
        'password-recovery/',
        views.public_auth_password_recovery,
        name="homepage-password-recovery"
    ),

    re_path(
        'rest-auth/registration/account-confirm-email/(?P<key>[-:\w]+)/$',
        views.public_auth_email_confirmation,
        name="account_confirm_email"
    ),

    re_path(
        'reset/(?P<uid>[-:\w]+)/(?P<token>[-:\w]+)/$',
        views.public_auth_password_reset,
        name="account_reset_password"
    ),

    path('rest-auth/', include('rest_auth.urls')),
    path('rest-auth/registration/', include('rest_auth.registration.urls')),

    
    path('subscribers/', views.SubscribersList.as_view(), name='subscribers'),
]
