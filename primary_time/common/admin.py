from django.contrib import admin

from .models import (
    CommonStaticPage,
    ProjectOfficer,
    ProjectAdvantage,
    AboutQuestion,
)


class CommonStaticPageAdmin(admin.ModelAdmin):
    list_display = (
        'title', 'slug'
    )
    prepopulated_fields = {'slug': ('title',)}


class ProjectOfficerAdmin(admin.ModelAdmin):
    list_display = (
        'name', 'is_active', 'specialty', 'order'
    )
    ordering = ['order']

class ProjectAdvantageAdmin(admin.ModelAdmin):
    list_display = (
        'title', 'is_active', 'order'
    )
    ordering = ['order']

    
class AboutQuestionAdmin(admin.ModelAdmin):
    list_display = (
        'id', 'created',
        'user_name', 'user_email', 'user_phone'
    )
    list_filter = ('user_email',)


admin.site.register(CommonStaticPage, CommonStaticPageAdmin)
admin.site.register(ProjectOfficer, ProjectOfficerAdmin)
admin.site.register(ProjectAdvantage, ProjectAdvantageAdmin)
admin.site.register(AboutQuestion, AboutQuestionAdmin)
