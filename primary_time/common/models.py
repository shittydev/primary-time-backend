from django.db import models
from autoslug import AutoSlugField
from django.utils.translation import ugettext_lazy as _
from common.utils.file import get_image_path
from ckeditor.fields import RichTextField
from ckeditor_uploader.fields import RichTextUploadingField
from phonenumber_field.modelfields import PhoneNumberField


class CommonStaticPage(models.Model):
    seo_title = models.CharField(
        max_length=255,
        verbose_name="SEO Title",
        default=""
    )
    seo_description = models.CharField(
        max_length=255,
        verbose_name="SEO Description",
        default=""
    )
    title = models.CharField(
        max_length=255,
        verbose_name="Title",
        default=""
    )
    content = RichTextUploadingField(
        verbose_name="Content",
        default=""
    )
    additional_content = RichTextUploadingField(
        verbose_name="Additional Content",
        default=""
    )
    slug = AutoSlugField(
        populate_from='title',
        verbose_name=_('slug'),
        default="",
        editable=True,
        unique=True
    )
    is_active = models.BooleanField(
        default=False
    )

    def __str__(self):
        return self.title

    class Meta:
        verbose_name = "Статическая страница"
        verbose_name_plural = "Статические страницы"
        ordering = ['title']


class ProjectOfficer(models.Model):
    name = models.CharField(
        max_length=255,
        verbose_name="Name",
        default=""
    )
    specialty = models.CharField(
        max_length=255,
        verbose_name="Specialty",
        default=""
    )
    short_description = RichTextField(
        verbose_name="Short Description",
        default=""
    )
    description = RichTextField(
        verbose_name="Description",
        default=""
    )
    created = models.DateTimeField(
        auto_now_add=True
    )
    image = models.ImageField(
        _('image'),
        upload_to=get_image_path,
        blank=True
    )
    is_active = models.BooleanField(
        default=False
    )
    order = models.IntegerField(
        default=0
    )

    class Meta:
        verbose_name = "Сотрудник проекта"
        verbose_name_plural = "Сотрудники проекта"
        ordering = ['order']


class ProjectAdvantage(models.Model):
    title = models.CharField(
        max_length=255,
        verbose_name="Title",
        default=""
    )
    description = models.CharField(
        max_length=255,
        verbose_name="Description",
        default=""
    )
    icon = models.ImageField(
        _('icon'),
        upload_to=get_image_path,
        blank=True
    )
    is_active = models.BooleanField(
        default=False
    )
    order = models.IntegerField(
        default=0
    )

    class Meta:
        verbose_name = "Преимущество проекта"
        verbose_name_plural = "Преимущества проекта"
        ordering = ['order']

class AboutQuestion(models.Model):
    created = models.DateTimeField(
        auto_now_add=True
    )
    user_name = models.CharField(
        blank=False,
        default='',
        max_length=255
    )
    user_phone = PhoneNumberField(
        blank=False
    )
    user_email = models.EmailField(
        blank=False,
        default='',
        max_length=255
    )

    def __str__(self):
        return str(self.user_phone)

    class Meta:
        verbose_name = "Запрос помощи"
        verbose_name_plural = "Запросы помощи"
        ordering = ['created']