from django.shortcuts import (
    render,
    get_object_or_404,
    render_to_response,
)
from django.views.generic import (
    ListView,
    DetailView,
)

from common.models import (
    CommonStaticPage,
    ProjectAdvantage,
    ProjectOfficer,
    AboutQuestion
)

from rest_framework.generics import (
    ListCreateAPIView,
)

from common.serializers import (
    AboutQuestionSerializer
)

from blog.models import (
    CommonBlogRecordCategory,
)

from django.db.models import (
    Count,
)


class AboutPageView(DetailView):
    model = CommonStaticPage
    template_name = 'common/about.html'
    category_list = CommonBlogRecordCategory.objects.annotate(
        num_articles=Count('articles')).filter(
        num_articles__gt=0, is_secret=False).order_by('title')

    def get(self, request, *args, **kwargs):
        page = get_object_or_404(
            CommonStaticPage,
            slug='about',
        )

        context = {
            'categories': self.category_list,
        }

        context['seo_title'] = page.seo_title
        context['seo_description'] = page.seo_description

        context['is_authenticated'] = request.user.is_authenticated
        context['page'] = page

        context['advantages'] = ProjectAdvantage.objects.filter(is_active=True)
        context['officers'] = ProjectOfficer.objects.filter(is_active=True)

        return render_to_response(template_name=self.template_name, context=context)


class AboutQuestionsList(ListCreateAPIView):
    queryset = AboutQuestion.objects.all()
    serializer_class = AboutQuestionSerializer
