from django.conf import settings
from django.contrib.sites.shortcuts import get_current_site
from django.core.mail import EmailMultiAlternatives
from django.template.loader import render_to_string

from apps.config.models import SystemConfiguration


def send_notify(template_prefix, obj=None, request=None, to=None):
    current_site = get_current_site(request)
    merge_data = {'current_site': current_site, 'object': obj}
    subject = render_to_string(
        template_prefix + '_subject.txt', merge_data, request)
    text_body = render_to_string(
        template_prefix + '_message.txt', merge_data, request)
    html_body = render_to_string(
        template_prefix + '_message.html', merge_data, request)
    msg = EmailMultiAlternatives(
        subject.replace('\n', ''), text_body,
        settings.DEFAULT_FROM_EMAIL, [to])
    msg.attach_alternative(html_body, 'text/html')
    msg.send()


def send_admin_notify(template_prefix, obj=None, request=None):
    conf = SystemConfiguration.get_solo()
    send_notify(template_prefix, obj, request, to=conf.admin_notify_email)
