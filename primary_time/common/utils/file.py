import os
from uuid import uuid4

IMAGE_EXTS = ('jpg', 'jpeg', 'png', 'gif')
DOCUMENT_EXTS = ('pdf', 'doc', 'docx', 'xls', 'xlsx')
VIDEO_EXTS = ('mpg', 'mp4', 'avi')
ALL_EXTS = IMAGE_EXTS + DOCUMENT_EXTS + VIDEO_EXTS


def get_file_extension(filename):
    return os.path.splitext(filename)[1][1:]


def get_file_folder(instance):
    return instance.__class__.__name__.lower()


def get_random_filename(filename):
    ext = filename.split('.')[-1]
    filename = '{}.{}'.format(uuid4(), ext)
    return filename


def get_full_random_filename(instance, filename):
    return os.path.join(
        get_file_folder(instance),
        get_random_filename(filename)
    )


def get_image_path(instance, filename):
    folder = get_file_folder(instance)
    sub_path = os.path.join(folder, get_random_filename(filename))
    return os.path.join('images', sub_path)