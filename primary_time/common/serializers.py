from phonenumber_field.modelfields import PhoneNumberField
from common.models import AboutQuestion
from rest_framework.serializers import (
    ModelSerializer,
    CharField,
)

from django.core.mail import mail_admins
from django.template.loader import get_template
from django.contrib.sites.models import Site


class AboutQuestionSerializer(ModelSerializer):
    user_phone = CharField(
        validators=PhoneNumberField().validators,
    )

    def create(self, validated_data):
        question = AboutQuestion(
            user_name=validated_data['user_name'],
            user_email=validated_data['user_email'],
            user_phone=validated_data['user_phone'],
        )
        template = get_template('common/emails/about-question.html')
        context = {'question': question,
                   'host': Site.objects.get_current().domain}
        content = template.render(context)

        mail_admins(
            'Задан вопрос',
            'Задан вопрос на странице "О нас"',
            html_message=content)
        question.save()
        return question

    class Meta:
        model = AboutQuestion
        fields = (
            'id', 'created',
            'user_name', 'user_email', 'user_phone'
        )
