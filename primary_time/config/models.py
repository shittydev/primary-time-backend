from django.db import models
from django.utils.translation import ugettext_lazy as _

from solo.models import SingletonModel


class SystemConfiguration(SingletonModel):
    admin_notify_email = models.EmailField(
        _('email for admin notifications'), blank=True)

    def __str__(self):
        return 'System Configuration'

    class Meta:
        verbose_name = 'System Configuration'


class AnalyticsConfiguration(SingletonModel):
    yandex_webmaster_code = models.CharField(
        max_length=255,
        verbose_name="Yandex webmaster code",
        blank=True,
        default=""
    )
    
    google_search_console_code = models.CharField(
        max_length=255,
        verbose_name="Google search console code",
        blank=True,
        default=""
    )
    
    yandex_metrika_number = models.CharField(
        max_length=255,
        verbose_name="Yandex metrika counter number",
        blank=True,
        default=""
    )
    
    google_analytics_number = models.CharField(
        max_length=255,
        verbose_name="Google analytics counter number",
        blank=True,
        default=""
    )

    def __str__(self):
        return 'Analytics'

    class Meta:
        verbose_name = 'Аналитика'

class SocialNetworksConfiguration(SingletonModel):
    vkontakte = models.CharField(
        max_length=255,
        verbose_name="Vkontakte link",
        blank=True,
        default=""
    )
    
    odnoklassniki = models.CharField(
        max_length=255,
        verbose_name="Odnoklassniki link",
        blank=True,
        default=""
    )
    
    instagram = models.CharField(
        max_length=255,
        verbose_name="Instagram link",
        blank=True,
        default=""
    )
    
    facebook = models.CharField(
        max_length=255,
        verbose_name="Facebook link",
        blank=True,
        default=""
    )
    
    telegram = models.CharField(
        max_length=255,
        verbose_name="Telegram link",
        blank=True,
        default=""
    )
    
    viber = models.CharField(
        max_length=255,
        verbose_name="Viber link",
        blank=True,
        default=""
    )

    def __str__(self):
        return 'Social Networks Configuration'

    class Meta:
        verbose_name = 'Социальные сети'
