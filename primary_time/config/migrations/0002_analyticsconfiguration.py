# Generated by Django 2.0.1 on 2018-04-02 16:16

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('config', '0001_initial'),
    ]

    operations = [
        migrations.CreateModel(
            name='AnalyticsConfiguration',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('yandex_webmaster_code', models.CharField(default='', max_length=255, verbose_name='Yandex webmaster code')),
                ('google_search_console_code', models.CharField(default='', max_length=255, verbose_name='Google search console code')),
                ('yandex_metrika_number', models.CharField(default='', max_length=255, verbose_name='Yandex metrika counter number')),
                ('google_analytics_number', models.CharField(default='', max_length=255, verbose_name='Google analytics counter number')),
            ],
            options={
                'verbose_name': 'Аналитика',
            },
        ),
    ]
