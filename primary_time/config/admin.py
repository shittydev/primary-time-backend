from django.contrib import admin

from solo.admin import SingletonModelAdmin

from .models import SystemConfiguration, AnalyticsConfiguration, SocialNetworksConfiguration

admin.site.register(SystemConfiguration, SingletonModelAdmin)
admin.site.register(AnalyticsConfiguration, SingletonModelAdmin)
admin.site.register(SocialNetworksConfiguration, SingletonModelAdmin)