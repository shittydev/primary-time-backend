import environ

ROOT_DIR = environ.Path(__file__) - 2

env = environ.Env()

# SECURITY WARNING: keep the secret key used in production secret!
SECRET_KEY = env('DJANGO_SECRET_KEY')

# SECURITY WARNING: don't run with debug turned on in production!
DEBUG = env.bool('DEBUG', False)
# DEBUG = env.bool('DEBUG', True)

ALLOWED_HOSTS = env('ALLOWED_HOSTS', default='127.0.0.1').split(',')

# Application definition

INSTALLED_APPS = [
    'django.contrib.admin',
    'django.contrib.auth',
    'django.contrib.contenttypes',
    'django.contrib.sessions',
    'django.contrib.messages',
    'django.contrib.staticfiles',
    'django.contrib.sites',

    'solo',

    # CORS
    'corsheaders',

    # REST
    'rest_framework',
    'rest_framework.authtoken',
    'rest_auth',
    'rest_auth.registration',

    'allauth',
    'allauth.account',
    'allauth.socialaccount',

    # Other libs
    'ckeditor',
    'ckeditor_uploader',
    'phonenumber_field',
    'easy_thumbnails',
    'braces',

    'config',

    'base',
    'blog',
    'common',
    'public_auth',
]

CKEDITOR_UPLOAD_PATH = "uploads/"

MIDDLEWARE = [
    'corsheaders.middleware.CorsMiddleware',
    'django.middleware.common.BrokenLinkEmailsMiddleware',
    'django.middleware.security.SecurityMiddleware',
    'django.contrib.sessions.middleware.SessionMiddleware',
    'django.middleware.csrf.CsrfViewMiddleware',
    'django.middleware.common.CommonMiddleware',
    'django.contrib.auth.middleware.AuthenticationMiddleware',
    'django.contrib.messages.middleware.MessageMiddleware',
    'django.middleware.clickjacking.XFrameOptionsMiddleware',
]

ROOT_URLCONF = 'primary_time.urls'

TEMPLATES = [
    {
        'BACKEND': 'django.template.backends.django.DjangoTemplates',
        'DIRS': [],
        'APP_DIRS': True,
        'OPTIONS': {
            'context_processors': [
                'django.template.context_processors.debug',
                'django.template.context_processors.request',
                'django.contrib.auth.context_processors.auth',
                'django.contrib.messages.context_processors.messages',
            ],
        },
    },
]

WSGI_APPLICATION = 'primary_time.wsgi.application'

# Database
# https://docs.djangoproject.com/en/2.0/ref/settings/#databases

DATABASES = {
    'default': {
        'ENGINE': 'django.db.backends.postgresql_psycopg2',
        'NAME': env('POSTGRES_NAME', default='primaryDB'),
        'USER': env('POSTGRES_USER', default='pt-admin'),
        'PASSWORD': env('POSTGRES_PASSWORD', default='qweqweqwe'),
        'HOST': env('POSTGRES_HOST', default='localhost'),
        'PORT': env('POSTGRES_PORT', default='5432'),
    }
}

# Password validation
# https://docs.djangoproject.com/en/2.0/ref/settings/#auth-password-validators

AUTH_PASSWORD_VALIDATORS = [
    {
        'NAME': 'django.contrib.auth.password_validation.UserAttributeSimilarityValidator',
    },
    {
        'NAME': 'django.contrib.auth.password_validation.MinimumLengthValidator',
    },
    {
        'NAME': 'django.contrib.auth.password_validation.CommonPasswordValidator',
    },
    {
        'NAME': 'django.contrib.auth.password_validation.NumericPasswordValidator',
    },
]

# Internationalization
# https://docs.djangoproject.com/en/2.0/topics/i18n/

# LANGUAGE_CODE = 'en-us'
LANGUAGE_CODE = 'ru-ru'

TIME_ZONE = 'UTC'

USE_I18N = True

USE_L10N = True

USE_TZ = True

# Static files (CSS, JavaScript, Images)
# https://docs.djangoproject.com/en/2.0/howto/static-files/

MEDIA_ROOT = str(ROOT_DIR('public', 'media'))
MEDIA_URL = '/media/'

STATIC_ROOT = str(ROOT_DIR('public', 'static'))
STATIC_URL = '/static/'

STATICFILES_DIRS = (
    str(ROOT_DIR('_static')),
)

# For allauth and rest auth
SITE_ID = 1

REST_AUTH_REGISTER_SERIALIZERS = {
    'REGISTER_SERIALIZER': 'public_auth.serializers.UserProfileRegisterSerializer',
    # 'USER_DETAILS_SERIALIZER': 'public_auth.serializers.UserSerializer',
}
ACCOUNT_USERNAME_REQUIRED = False
CORS_ORIGIN_ALLOW_ALL = True

# For depolyment
# STATIC_ROOT = "/var/www/example.com/static/"

# EMAIL_BACKEND = 'django.core.mail.backends.console.EmailBackend'
EMAIL_HOST = 'smtp.gmail.com'
EMAIL_HOST_USER = 'primarytimeru@gmail.com'
EMAIL_HOST_PASSWORD = 'moorwort sedulity'
EMAIL_PORT = 587
EMAIL_USE_TLS = True
EMAIL_BACKEND = 'django.core.mail.backends.smtp.EmailBackend'

SERVER_EMAIL = 'root@primarytime.com'

# ----
REST_SESSION_LOGIN = True
SITE_ID = 1

AUTH_USER_MODEL = 'public_auth.User'

# ACCOUNT_EMAIL_REQUIRED = False

# ACCOUNT_AUTHENTICATION_METHOD = 'username'

# ACCOUNT_EMAIL_VERIFICATION = 'optional'


# CSRF_HEADER_NAME='X-CSRF-TOKEN'
CSRF_USE_SESSIONS = False

THUMBNAIL_ALIASES = {
    '': {
        'blog_image_detail': {'size': (865, 865), 'crop': True},
        'blog_image_large': {'size': (600, 600), 'crop': True},
        'blog_image_medium': {'size': (300, 300), 'crop': True},
    },
}


ADMINS = []
MANAGERS = ADMINS