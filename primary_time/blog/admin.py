from django.contrib import admin

from .models import CommonBlogRecord, CommonBlogRecordCategory, BlogRecordComment, BlogRecordQuestion


class CommonBlogRecordCategoryAdmin(admin.ModelAdmin):
    list_display = (
        'title', 'slug')
    list_filter = ('title',)
    prepopulated_fields = {'slug': ('title',)}


class CommonBlogRecordAdmin(admin.ModelAdmin):
    list_display = (
        'title', 'slug', 'category', 'is_active',
        'created', 'modified'
    )
    list_filter = ('is_active', 'publish_date', 'created')
    prepopulated_fields = {'slug': ('title',)}


class BlogRecordCommentAdmin(admin.ModelAdmin):
    list_display = (
        'author', 'article', 'created',
        'approved_comment'
    )
    list_filter = ('created', 'approved_comment')


class BlogRecordQuestionAdmin(admin.ModelAdmin):
    list_display = (
        'id', 'created', 'article',
        'user_name', 'user_email', 'user_phone'
    )
    list_filter = ('user_email', 'article_id')


admin.site.register(CommonBlogRecord, CommonBlogRecordAdmin)
admin.site.register(CommonBlogRecordCategory, CommonBlogRecordCategoryAdmin)
admin.site.register(BlogRecordComment, BlogRecordCommentAdmin)
admin.site.register(BlogRecordQuestion, BlogRecordQuestionAdmin)
