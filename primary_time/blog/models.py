from django.conf import settings
from django.db import models
from django.contrib.postgres.fields import ArrayField
from django.utils import timezone
from django.utils.translation import ugettext_lazy as _
from datetime import datetime

# Specifics fields
from autoslug import AutoSlugField
from phonenumber_field.modelfields import PhoneNumberField
from model_utils.models import TimeStampedModel
from ckeditor.fields import RichTextField
from ckeditor_uploader.fields import RichTextUploadingField

from public_auth.models import User

from common.utils.file import get_image_path


class CommonBlogRecordCategory(models.Model):
    seo_title = models.CharField(
        max_length=255,
        verbose_name="SEO Title",
        default=""
    )
    seo_description = models.CharField(
        max_length=255,
        verbose_name="SEO Description",
        default=""
    )
    title = models.CharField(
        max_length=255,
        verbose_name="Title",
        default=""
    )
    slug = AutoSlugField(
        populate_from='title',
        verbose_name=_('slug'),
        default="",
        editable=True,
        unique=True
    )
    is_secret = models.BooleanField(
        default=False,
        verbose_name="Скрытая категория"
    )
    @property
    def active_articles(self):
        return self.articles.filter(is_active=True)


    def __str__(self):
        return self.title

    class Meta:
        verbose_name = "Категория"
        verbose_name_plural = "Категории"
        ordering = ['title']


class BlogRecordComment(models.Model):
    created = models.DateTimeField(
        auto_now_add=True
    )
    author = models.ForeignKey(
        User,
        on_delete=models.CASCADE,
        verbose_name=_('author')
    )
    article = models.ForeignKey(
        'blog.CommonBlogRecord',
        on_delete=models.CASCADE,
        related_name='comments'
    )
    content = models.TextField(
        default=''
    )
    approved_comment = models.BooleanField(
        default=False
    )

    def __str__(self):
        return self.content

    class Meta:
        verbose_name = "Комментарий"
        verbose_name_plural = "Комментарии"
        ordering = ['created']


class BlogRecordQuestion(models.Model):
    created = models.DateTimeField(
        auto_now_add=True
    )
    user_name = models.CharField(
        blank=False,
        default='',
        max_length=255
    )
    user_phone = PhoneNumberField(
        blank=False
    )
    user_email = models.EmailField(
        blank=False,
        default='',
        max_length=255
    )
    article = models.ForeignKey(
        'blog.CommonBlogRecord',
        on_delete=models.CASCADE,
        related_name='questions'
    )
    question_content = models.TextField(
        default=''
    )

    def __str__(self):
        return self.question_content

    class Meta:
        verbose_name = "Вопрос к статье"
        verbose_name_plural = "Вопросы к статьям"
        ordering = ['created']


class AbstractBlogRecord(TimeStampedModel):
    seo_title = models.CharField(
        max_length=255,
        verbose_name="SEO Title",
        default=""
    )
    seo_description = models.CharField(
        max_length=255,
        verbose_name="SEO Description",
        default=""
    )
    created = models.DateTimeField(
        auto_now_add=True
    )
    title = models.CharField(
        _('title'),
        max_length=255
    )
    slug = AutoSlugField(
        populate_from='title',
        verbose_name=_('slug'),
        editable=True
    )
    publish_date = models.DateTimeField(
        _('publish date')
    )
    is_active = models.BooleanField(
        _('is active'),
        default=False
    )
    content = RichTextUploadingField()
    image = models.ImageField(
        _('image'),
        upload_to=get_image_path,
        blank=True
    )
    def get_absolute_url(self):
        return "/blog/{category}/{slug}/".format(category=self.category.slug, slug=self.slug)

    class Meta:
        abstract = True


class CommonBlogRecord(AbstractBlogRecord):
    intro = RichTextField()
    category = models.ForeignKey(
        CommonBlogRecordCategory,
        models.SET_NULL,
        related_name='articles',
        verbose_name="Категория",
        blank=True,
        null=True
    )

    @property
    def approved_comments_count(self):
        return self.comments.filter(approved_comment=True).count()

    @property
    def comments_count(self):
        return self.comments.count()

    def __str__(self):
        return self.title

    class Meta:
        verbose_name = _('Статья в блоге')
        verbose_name_plural = _('Статьи в блоге')


class ArticleBlogRecordView(models.Model):
    article = models.ForeignKey(
        CommonBlogRecord,
        on_delete=models.CASCADE,
        related_name='views'
    )
    ip = models.CharField(
        max_length=40
    )
    session = models.CharField(
        max_length=40
    )
    created = models.DateTimeField(
        default=datetime.now()
    )
