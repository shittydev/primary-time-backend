from phonenumber_field.modelfields import PhoneNumberField
from blog.models import BlogRecordComment, CommonBlogRecord, BlogRecordQuestion
from easy_thumbnails.exceptions import InvalidImageFormatError
from easy_thumbnails.files import get_thumbnailer
from rest_framework.serializers import (
    ModelSerializer,
    CharField,
    URLField,
    ReadOnlyField,
    SerializerMethodField,
)

from django.core.mail import mail_admins
from django.template.loader import get_template
from django.contrib.sites.models import Site


class BlogArticleSerializer(ModelSerializer):
    category_name = CharField(source='category.title')
    category_slug = CharField(source='category.slug')
    views_count = SerializerMethodField()
    image_src = SerializerMethodField()

    def get_views_count(self, obj):
        return obj.views.count()

    def get_image_src(self, obj):
        image_field = getattr(obj, self.Meta.thumbnail_source)
        if image_field:
            thumbnailer = get_thumbnailer(image_field)
            thumbnail_options = {
                'size': self.Meta.thumbnail_size
            }

            if (
                hasattr(self.Meta, 'thumbnail_crop') and
                self.Meta.thumbnail_crop
            ):
                thumbnail_options['crop'] = True

            try:
                return thumbnailer.get_thumbnail(thumbnail_options).url
            except InvalidImageFormatError:
                return ''
        else:
            return ''

    class Meta:
        model = CommonBlogRecord
        thumbnail_size = (600, 600)
        thumbnail_crop = True
        thumbnail_source = 'image'
        fields = (
            'title', 'slug', 'intro',
            'category_name', 'category_slug',
            'views_count', 'approved_comments_count',
            'image_src'
        )


class BlogRecordCommentSerializer(ModelSerializer):
    author = ReadOnlyField(source='author.username')
    article_id = ReadOnlyField()

    def create(self, validated_data):
        comment = BlogRecordComment(
            author=validated_data['author'],
            content=validated_data['content'],
            article=CommonBlogRecord.objects.get(
                id=validated_data['article_id'])
        )
        comment.save()
        return comment

    class Meta:
        model = BlogRecordComment
        fields = (
            'id', 'created', 'author', 'content',
            'approved_comment', 'article_id'
        )


class BlogRecordQuestionSerializer(ModelSerializer):
    article_id = ReadOnlyField()
    user_phone = CharField(
        validators=PhoneNumberField().validators,
    )

    def create(self, validated_data):
        article = CommonBlogRecord.objects.get(
            id=validated_data['article_id'])
        question = BlogRecordQuestion(
            user_name=validated_data['user_name'],
            user_email=validated_data['user_email'],
            user_phone=validated_data['user_phone'],
            question_content=validated_data['question_content'],
            article=article
        )
        question.save()

        template = get_template('common/emails/article-question.html')
        context = {'article': article, 'question': question, 'host': Site.objects.get_current().domain}
        content = template.render(context)

        mail_admins(
            'Задан вопрос к статье',
            'Задан вопрос к статье',
            html_message=content)

        return question

    class Meta:
        model = BlogRecordQuestion
        fields = (
            'id', 'created',
            'user_name', 'user_email', 'user_phone', 'question_content',
            'article_id'
        )
