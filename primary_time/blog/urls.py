from django.urls import path
from . import views
from django.views.generic import (
    ListView,
    DetailView
)
from blog.models import (
    CommonBlogRecordCategory,
    CommonBlogRecord
)

urlpatterns = [
    path('<article_id>/comments/', views.CommentsList.as_view(), name='comments'),
    path('<category_id>/articles/',
         views.CategoryArticlesList.as_view(), name='articles'),
    path('<article_id>/questions/',
         views.QuestionsList.as_view(), name='questions'),
    path('', views.blog_index, name='blog-index'),
    path('<category_slug>/', views.BlogCategoryView.as_view(), name='blog-category'),
    path('<category_slug>/<article_slug>/',
         views.BlogArticleView.as_view(), name='blog-article'),
]
