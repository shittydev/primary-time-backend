from datetime import datetime
from django.template.context_processors import csrf

from django.shortcuts import (
    render,
    get_object_or_404,
    render_to_response,
)
from django.views.generic import (
    ListView,
    DetailView,
)

from rest_framework.generics import (
    ListCreateAPIView,
    ListAPIView,
)
from common.models import (
    CommonStaticPage,
)
from blog.models import (
    CommonBlogRecordCategory,
    CommonBlogRecord,
    BlogRecordComment,
    BlogRecordQuestion,
    ArticleBlogRecordView,
)
from blog.serializers import (
    BlogRecordCommentSerializer,
    BlogRecordQuestionSerializer,
    BlogArticleSerializer,
)
from common.paginations import FiveResultsSetPagination

from django.db.models import (
    Count,
)

# Common views
# -----------------------------


def blog_index(request):
    category_list = CommonBlogRecordCategory.objects.annotate(
        num_articles=Count('articles')).filter(
        num_articles__gt=0).filter(is_secret=False).order_by('title')
    page = get_object_or_404(
        CommonStaticPage,
        slug='blog',
    )
    article_list = CommonBlogRecord.objects.filter(
        is_active=True).order_by('-publish_date')[:3]
    context_dict = {
        'categories': category_list,
        'articles': article_list,
        'seo_title': page.seo_title,
        'seo_description': page.seo_description,
        'page': page
    }
    return render(request, 'blog/index.html', context_dict)


class BlogCategoryView(ListView):
    model = CommonBlogRecordCategory
    template_name = 'blog/category.html'

    def get_queryset(self):
        self.category = get_object_or_404(
            CommonBlogRecordCategory,
            slug=self.kwargs['category_slug'])
        return CommonBlogRecord.objects.filter(
            category=self.category.id,
        ).order_by(
            '-publish_date'
        )[:5]

    def get_context_data(self, **kwargs):
        context = super(BlogCategoryView, self).get_context_data(**kwargs)
        context['category'] = self.category
        context['seo_title'] = self.category.seo_title
        context['seo_description'] = self.category.seo_description
        context['categories'] = CommonBlogRecordCategory.objects.annotate(
            num_articles=Count('articles')
        ).filter(
            num_articles__gt=0
        ).filter(
            is_secret=False
        ).order_by(
            'title'
        )
        context['articles'] = CommonBlogRecord.objects.all().filter(
            is_active=True
        ).order_by(
            '-publish_date'
        )[:3]
        context['is_category_has_more_articles'] = self.category.articles.filter(
            is_active=True
        ).count() > 5

        return context


class BlogArticleView(DetailView):
    model = CommonBlogRecord
    template_name = 'blog/article.html'

    def get(self, request, *args, **kwargs):
        article = CommonBlogRecord.objects.get(slug=kwargs['article_slug'])

        if not request.session.session_key:
            request.session.save()

        if not ArticleBlogRecordView.objects.filter(
                article=article,
                session=request.session.session_key
        ):
            view = ArticleBlogRecordView(
                article=article,
                ip=request.META['REMOTE_ADDR'],
                created=datetime.now(),
                session=request.session.session_key
            )
            view.save()

        context = {}
        context.update(csrf(request))

        context['seo_title'] = article.seo_title
        context['seo_description'] = article.seo_description

        context['is_authenticated'] = request.user.is_authenticated
        context['article'] = article
        context['views_count'] = article.views.count()

        context['next_article'] = CommonBlogRecord.objects.filter(
            id__gt=article.id,
            category=article.category
        ).order_by('id').first()
        
        context['previous_article'] = CommonBlogRecord.objects.filter(
            id__lt=article.id,
            category=article.category
        ).order_by('id').first()

        # context['comments_count'] = article.comments.filter(
        #     approved_comment=True
        # ).count()

        # context['comments'] = article.comments.filter(
        #     approved_comment=True
        # ).order_by(
        #     '-created'
        # )[:5]

        # context['is_article_has_more_comments'] = article.comments.filter(
        #     approved_comment=True
        # ).count() > 5

        context['comments_count'] = article.comments.count()

        context['comments'] = article.comments.order_by(
            '-created'
        )[:5]
        
        context['is_article_has_more_comments'] = article.comments.count() > 5
        
        context['categories'] = CommonBlogRecordCategory.objects.annotate(
            num_articles=Count('articles')
        ).filter(
            num_articles__gt=0
        ).filter(
            is_secret=False
        ).order_by(
            'title'
        )
        context['articles'] = CommonBlogRecord.objects.filter(
            is_active=True,
            category=article.category
        ).exclude(
            id=article.id
        ).order_by(
            '-publish_date'
        )[:3]
        context['relevant_articles'] = CommonBlogRecord.objects.filter(
            is_active=True,
            category=article.category
        ).exclude(
            id=article.id
        ).order_by(
            '-publish_date'
        )[:3]

        return render_to_response(template_name=self.template_name, context=context)

# API
# -----------------------------


class CategoryArticlesList(ListAPIView):
    serializer_class = BlogArticleSerializer
    pagination_class = FiveResultsSetPagination

    def get_queryset(self):
        category_id = self.kwargs['category_id']

        return CommonBlogRecord.objects.filter(
            category=category_id,
            is_active=True,
            is_secret=False
        ).order_by(
            '-publish_date'
        )


class CommentsList(ListCreateAPIView):
    serializer_class = BlogRecordCommentSerializer
    pagination_class = FiveResultsSetPagination

    def get_queryset(self):
        article_id = self.kwargs['article_id']

        return BlogRecordComment.objects.filter(
            article=article_id,
            approved_comment=True
        ).order_by(
            '-created'
        )

    def perform_create(self, serializer):
        serializer.save(
            author=self.request.user,
            article_id=self.kwargs.get('article_id'),
        )


class QuestionsList(ListCreateAPIView):
    queryset = BlogRecordQuestion.objects.all()
    serializer_class = BlogRecordQuestionSerializer

    def perform_create(self, serializer):
        serializer.save(
            author=self.request.user,
            article_id=self.kwargs.get('article_id'),
        )
