# Generated by Django 2.0.1 on 2018-02-25 19:05

import datetime
from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('blog', '0036_auto_20180225_2145'),
    ]

    operations = [
        migrations.AlterField(
            model_name='articleblogrecordview',
            name='created',
            field=models.DateTimeField(default=datetime.datetime(2018, 2, 25, 22, 5, 0, 621780)),
        ),
    ]
