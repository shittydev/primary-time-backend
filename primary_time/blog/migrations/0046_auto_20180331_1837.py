# Generated by Django 2.0.1 on 2018-03-31 15:37

import datetime
from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('blog', '0045_auto_20180331_1837'),
    ]

    operations = [
        migrations.AlterField(
            model_name='articleblogrecordview',
            name='created',
            field=models.DateTimeField(default=datetime.datetime(2018, 3, 31, 18, 37, 17, 313057)),
        ),
    ]
