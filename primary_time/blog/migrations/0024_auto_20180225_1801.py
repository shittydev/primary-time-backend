# Generated by Django 2.0.1 on 2018-02-25 15:01

import common.utils.file
import datetime
from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('blog', '0023_auto_20180225_1737'),
    ]

    operations = [
        migrations.AlterField(
            model_name='articleblogrecordview',
            name='created',
            field=models.DateTimeField(default=datetime.datetime(2018, 2, 25, 18, 1, 56, 101634)),
        ),
        migrations.AlterField(
            model_name='commonblogrecord',
            name='image',
            field=models.ImageField(blank=True, upload_to=common.utils.file.get_image_path, verbose_name='image'),
        ),
    ]
